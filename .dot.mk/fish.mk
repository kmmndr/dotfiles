fish_paths=\
	.config/fish/completions/kd.fish \
	.config/fish/completions/ry.fish \
	.config/fish/config.fish \
	.config/fish/functions/fish_user_key_bindings.fish \
	.config/fish/functions/fish_title.fish \
	.config/fish/functions/fish_right_prompt.fish \
	.config/fish/functions/developer_mode.fish \
	.config/fish/functions/dotenv.fish \
	.config/fish/functions/kd.fish \
	.config/fish/functions/ssh-agent_start.fish \
	.config/fish/functions/reload.fish \
	.config/fish/functions/PASS.fish \
	.config/fish/functions/fish_prompt.fish

.PHONY: fish
fish: fish-difftool
