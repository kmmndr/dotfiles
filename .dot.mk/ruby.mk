ruby_paths=\
	.gemrc \
	.irbrc \
	.pryrc

.PHONY: ruby
ruby: ruby-difftool
