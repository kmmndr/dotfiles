tmux_paths=\
	.tmux.conf

${HOME_FOLDER}/.tmux/plugins/tpm:
	mkdir -p ${HOME_FOLDER}/.tmux/plugins
	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
	@echo "Start tmux and type prefix+I to install plugins"

.PHONY: tmux
tmux: ${HOME_FOLDER}/.tmux/plugins/tpm tmux-difftool
