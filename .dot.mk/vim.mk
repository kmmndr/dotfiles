vim_paths=\
	.config/nvim/init.vim \
	.vimrc

${HOME_FOLDER}/.vim/pack/minpac/opt/minpac:
	mkdir -p $(shell dirname $@)
	git clone https://github.com/k-takata/minpac.git $@

.PHONY: vim
vim: ${HOME_FOLDER}/.vim/pack/minpac/opt/minpac vim-difftool
	vim +PackUpdate +qall
