fluxbox_paths=\
	.fluxbox/apps \
	.fluxbox/init \
	.fluxbox/keys \
	.fluxbox/lastwallpaper \
	.fluxbox/menu \
	.fluxbox/overlay \
	.fluxbox/startup \
	.fluxbox/windowmenu

fluxbox: fluxbox-difftool
