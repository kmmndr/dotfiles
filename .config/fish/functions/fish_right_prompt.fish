function fish_right_prompt
  if test $CMD_DURATION
    # Show duration of the last command in seconds
    if [ $CMD_DURATION -gt 1000 ]
      set duration (echo "$CMD_DURATION 1000" | awk '{printf "%.3fs", $1 / $2}')
      echo "[$duration]"
    end
  end
end
