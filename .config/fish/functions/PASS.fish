set -x PASSWORD_STORE_BASE ~/.pass

function PASS -S
  # if ! set -q PASSWORD_STORE_BASE
  #   set -x PASSWORD_STORE_BASE ~/.pass
  # end

  if [ (count $argv) -ge 1 ]
    set store "$PASSWORD_STORE_BASE/$argv[1]"
    echo "Exporting PASSWORD_STORE_DIR=$store"
    set -x PASSWORD_STORE_DIR $store
    mkdir -p $store
  else
    # tree -L 1 $PASSWORD_STORE_BASE | sed -e '1 s/^.*$/Stores/; $ d'
    echo 'Stores'
    cd $PASSWORD_STORE_BASE
    for id in (find . -name '.gpg-id' | sort)
       set store (dirname $id | sed 's/^.\//- /')
       echo $store
    end
  end
end

complete -x -c PASS -d "Store" -a "(ls $PASSWORD_STORE_BASE)"
